/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.acme.group;

import jakarta.persistence.Entity;
import static jakarta.persistence.FetchType.LAZY;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import java.util.Set;
import org.acme.faculty.Faculty;
import org.acme.professor.Professor;

/**
 *
 * @author student
 */
@Entity
public class Group {
    @Id
    @GeneratedValue
    public Long id;

    public String field;
    
    private String number;

    /**
     * Get the value of number
     *
     * @return the value of number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Set the value of number
     *
     * @param number new value of number
     */
    public void setNumber(String number) {
        this.number = number;
    }

        private String speciality;

    /**
     * Get the value of speciality
     *
     * @return the value of speciality
     */
    public String getSpeciality() {
        return speciality;
    }

    /**
     * Set the value of speciality
     *
     * @param speciality new value of speciality
     */
    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

        private String studentAmount;

    /**
     * Get the value of studentAmount
     *
     * @return the value of studentAmount
     */
    public String getStudentAmount() {
        return studentAmount;
    }

    /**
     * Set the value of studentAmount
     *
     * @param studentAmount new value of studentAmount
     */
    public void setStudentAmount(String studentAmount) {
        this.studentAmount = studentAmount;
    }

    //@ManyToOne(fetch=LAZY)
   // Faculty f1;
    
    //@ManyToMany
    //Set<Professor> p1;
}
