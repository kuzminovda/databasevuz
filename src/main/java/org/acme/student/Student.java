/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.acme.student;

import jakarta.persistence.Entity;
import static jakarta.persistence.FetchType.LAZY;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
//import org.acme.group.Group;

/**
 *
 * @author student
 */
@Entity

public class Student {
    @Id
    @GeneratedValue
    public Long id;

    public String field;
    
    private String firstName;

    /**
     * Get the value of firstName
     *
     * @return the value of firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the value of firstName
     *
     * @param firstName new value of firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

        private String lastName;

    /**
     * Get the value of lastName
     *
     * @return the value of lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the value of lastName
     *
     * @param lastName new value of lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

        private String birthDate;

    /**
     * Get the value of birthDate
     *
     * @return the value of birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Set the value of birthDate
     *
     * @param birthDate new value of birthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

        private String adress;

    /**
     * Get the value of adress
     *
     * @return the value of adress
     */
    public String getAdress() {
        return adress;
    }

    /**
     * Set the value of adress
     *
     * @param adress new value of adress
     */
    public void setAdress(String adress) {
        this.adress = adress;
    }

        private String speciality;

    /**
     * Get the value of speciality
     *
     * @return the value of speciality
     */
    public String getSpeciality() {
        return speciality;
    }

    /**
     * Set the value of speciality
     *
     * @param speciality new value of speciality
     */
    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

        private String course;

    /**
     * Get the value of course
     *
     * @return the value of course
     */
    public String getCourse() {
        return course;
    }

    /**
     * Set the value of course
     *
     * @param course new value of course
     */
    public void setCourse(String course) {
        this.course = course;
    }

    //@ManyToOne(fetch=LAZY)
    //Group group;
}
