/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.acme.faculty;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.Set;
import org.acme.group.Group;

/**
 *
 * @author student
 */
@Entity
public class Faculty {
    @Id
    @GeneratedValue
    public Long id;

    public String field;
    
    private String Name;

    /**
     * Get the value of Name
     *
     * @return the value of Name
     */
    public String getName() {
        return Name;
    }

    /**
     * Set the value of Name
     *
     * @param Name new value of Name
     */
    public void setName(String Name) {
        this.Name = Name;
    }

        private String dean;

    /**
     * Get the value of dean
     *
     * @return the value of dean
     */
    public String getDean() {
        return dean;
    }

    /**
     * Set the value of dean
     *
     * @param dean new value of dean
     */
    public void setDean(String dean) {
        this.dean = dean;
    }

        private String adress;

    /**
     * Get the value of adress
     *
     * @return the value of adress
     */
    public String getAdress() {
        return adress;
    }

    /**
     * Set the value of adress
     *
     * @param adress new value of adress
     */
    public void setAdress(String adress) {
        this.adress = adress;
    }

    //@OneToMany(mappedBy="faculty")
   // Set<Group> g1;
}
